#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jul  16 05:07:27 2021

@author: Angel Vázquez-Patiño
"""

from matplotlib import pyplot as plt
from matplotlib.widgets import Slider, Button

fig = plt.figure(figsize=(5, 3))
ax1 = plt.axes((0, 0, 0.8, 1))
ax2 = plt.axes((0.85, 0.2, 0.05, 0.8))
ax3 = plt.axes((0.95, 0.2, 0.05, 0.8))
ax4 = plt.axes((0.85, 0, 0.15, 0.1))

barras = ax1.bar((1, 2), (10, 3))

alt1 = Slider(ax=ax2,
             label='B1',
             valmin=0,
             valmax=10,
             valinit=10,
             valstep=1,
             orientation='vertical')

def actualizar1(valor):
    altura = alt1.val
    barras[0].set_height(altura)
    fig.canvas.draw_idle()

alt1.on_changed(actualizar1)

alt2 = Slider(ax=ax3,
             label='B2',
             valmin=0,
             valmax=10,
             valinit=3,
             valstep=1,
             orientation='vertical')

def actualizar2(valor):
    altura = alt2.val
    barras[1].set_height(altura)
    fig.canvas.draw_idle()

alt2.on_changed(actualizar2)

bot1 = Button(ax=ax4,
             label='Reiniciar')

def reiniciar(evento):
    alt1.reset()
    alt2.reset()

bot1.on_clicked(reiniciar)

plt.show()